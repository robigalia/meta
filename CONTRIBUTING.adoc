= How to Contribute

Thank you for your interest in contributing to the Robigalia project! There
are many ways to contribute, and we appreciate all of them. Opening issues,
discussing design, requesting features, writing documentation, changing the
code, writing blog posts, and creating logos/artwork for projects are all very
valuable.

If you have questions, please join our matrix room
https://matrix.to/#/%23robigalia:matrix.org[#robigalia:matrix.org] or hop on
IRC https://webchat.oftc.net/?channels=%23robigalia[#robigalia at OFTC].

As a reminder, all participants in our community are expected to honor our
https://rbg.systems/conduct.html[code of conduct].

[[feature-proposal]]
== Feature Proposal

To propose a change to the way that Robigo or any other Robigalia codebase
works, please open an issue directly on the relevant repository. Include the
text, `[feature-proposal]` in the title and someone will come by and re-tag
the issue appropriately. Here's a template you can use to file a feature
proposal, though it's not necessary to use it exactly:

....
One-paragraph summary

# Details

As many details as you think are necessary to communicate how the feature
should work.

# Benefits

Benefits of the feature.

# Drawbacks

Drawbacks of the feature.
....

There is https://gitlab.com/robigalia/meta/issues/1[an example feature proposal]
if you want to see what it looks like.

[[bug-reports]]
== Bug Reports

While bugs are unfortunate, they're a reality in software. We can't fix what we
don't know about, so please report liberally. If you're not sure if something
is a bug or not, feel free to file a bug anyway.

**If you believe reporting your bug publicly represents a security risk to
users of software developed by the Robigalia project, please follow our
https://rbg.systems/security.html[instructions for reporting security
vulnerabilities]**.

If you have the chance, before reporting a bug, please
https://gitlab.com/groups/robigalia/issues[search existing issues] as it's
possible that someone else has already reported your error. This doesn't always
work, and sometimes it's hard to know what to search for, so consider this
extra credit. We won't mind if you accidentally file a duplicate report, we'll
typically close it and point out which issue is a duplicate of.

Opening an issue is as easy as following
https://gitlab.com/groups/robigalia/issues[this link] and filling out the
fields after clicking "New Issue". Here's a template that you can use to file a
bug, though it's not necessary to use it exactly:

....
<short summary of the bug>

I tried this:

<testcase demonstrating the bug>

I expected to see this happen: <explanation>

Instead, this happened: <explanation>
....

All three components are important: what you did, what you expected, what
happened instead.

[[submitting-code]]
== Submitting code

To coordinate changes to our software, we use a feature of GitLab called
"merge requests".

[[commit-messages]]
== Commit messages

Commit messages should be self-contained and describe the motivation for a
change. The subject line should be short, with any elaborations in the body.
If the commit closes an issue, write "Closes #issuenumber" at the end of the
body.

For some guidelines, read http://chris.beams.io/posts/git-commit/[this] for
the basics, and https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message[this]
to become a pro.

[[commit-signing]]
== Commit signing

For a https://mikegerwitz.com/papers/git-horror-story[variety of reasons],
we use commit signing to verify the authenticity and authorship of changes
to our projects. If you don't already have one, create a PGP key. There are
many ways to do this.
https://alexcabal.com/creating-the-perfect-gpg-keypair/[This blog post] by
Alex Cabal has a decent explanation of how to do it and some of thetradeoffs
involved. This key doesn't need to be used for other purposes, although you
should consider using it for emails to the robigalia-dev mailing list!

Once you have a keypair and it is uploaded to the keyservers,
https://mikegerwitz.com/papers/git-horror-story#trust-ensure[learn how to
use gpg with git]. Please sign all commits you intend to include in a
merge request.

[[forking-the-repositories.]]
== Forking the repositories.

We use a custom system for managing our large amount of repositories. We have
one "master" repository, https://gitlab.com/robigalia/devbox[devbox], which
contains scripts for fetching the other repositories. It also includes a
https://www.vagrantup.com/[Vagrant] configuration, which is the easiest way
to get started with Rust and seL4. See the readme in that repository to see
how to use the scripts and, optionally, set up Vagrant. Part of the setup is
running the `add_remotes.sh` script, which will rename the official repo to
`upstream` and add an `origin` remote pointing to a repo under your own username.

To fork a repository, navigate to it on GitLab and click the "Fork project"
button", which is to the left of the repository URL:

image:https://gitlab.com/robigalia/meta/raw/master/fork_button.png[fork project button location image]

Then, you can interact with that repository using the `origin` remote in the
relevant sub-repository of `devbox`.

[[recognize-the-developer-certificate-of-origin]]
== Recognize the Developer Certificate of Origin

Before we can accept code you have written into our repositories, you must
agree to the Developers Certificate of Origin. You can view it
https://gitlab.com/robigalia/meta/blob/master/DCO.txt[in this repository].
It is quite minimalist, and is also used by the Linux kernel.

Add yourself to the .mailmap file in this repository, with the name you'd like
to be called by and all of the email addresses you'll use (you can add to this
later), as well as your PGP key fingerprint in a comment.

Create a new commit with those changes and send a merge request.

[[submitting-a-merge-request]]
== Submitting a merge request

To submit a merge request, push the commits you want to include to a branch on
your fork of the relevant repository. Refer to the
http://doc.gitlab.com/ce/gitlab-basics/add-merge-request.html[official GitLab
documentation] to see how to proceed from there.

Please open merge requests against the `master` branch. If needed, we will
merge into a separate branch manually.

All merge requests are reviewed by another person.

If you want to request that a specific person reviews your pull request, you
can specify the "assignee" when you create the pull request. For example,
Corey usually reviews changes to sel4-sys, so you could specify him as the
assignee if you send a merge request to sel4-sys.

[[writing-documentation]]
== Writing Documentation

Documentation improvements are very welcome. The source of rbg.systems is
in its https://gitlab.com/robigalia/rbg.systems[own repository], and
individual crate documentation is hosted on
https://doc.rbg.systems[doc.rbg.systems].

Documentation merge requests function in the same way as other merge requests.

[[helpful-links-and-information]]
== Helpful Links and Information

* The https://sel4.systems/[seL4 website] has things like the manual and how
to build seL4.
* The https://rust-lang.org/[Rust website] for all your Rust-related
concerns.

_This document adapted from https://github.com/rust-lang/rust/blob/master/CONTRIBUTING.md_
