= Robigalia

Robigalia is a project with two goals:

1.  Build a robust Rust ecosystem around seL4
2.  Create a highly reliable persistent capability OS, continuing the heritage of EROS and Coyotos

We're currently working away at goal 2, as a way to further goal 1. 

To get started with Rust and seL4, first https://rbg.systems/build-environment.html[set up a build environment]. 
From there, the sky's the limit!
We're currently working on a tutorial and some examples while the OS is being created.
In the meantime, https://rbg.systems/contributing.html[maybe you'd like to contribute?]

// Then, check out the https://rbg.systems/tutorial.html[tutorial]
// or https://rbg.systems/examples.html[examples] to see how you can start
// building Rust applications.

Want to know more about Rust, and why it's useful in this context?
https://rbg.systems/why-rust.html[Go here].

Want to know more about microkernels and seL4?
https://rbg.systems/about-microkernels.html[We have that too].

* https://gitlab.com/robigalia/meta/issues[Our project-wide issue tracker]
* https://rbg.systems/contributing.html[Contributing]
* https://rbg.systems/dco.html[Certificate of origin]
* https://rbg.systems/build-environment.html[Setting up a build environment]
* https://rbg.systems/examples.html[Examples]
// * https://rbg.systems/tutorial.html[Tutorial]
