[[setting-up-a-build-environment]]
Setting up a build environment
------------------------------

Setting up a build environment for Robigalia is easy! First, clone the "devbox" repository:

    git clone --recursive https://gitlab.com/robigalia/devbox.git robigalia && cd robigalia && git submodule update --init --remote

Set the `RUST_TARGET_PATH` environment variable in your shell configuration to
`path/to/robigalia/sel4-targets`. Be sure to either source the new
configuration, set the environment variable in your current shell, or start a
new shell. Something like this in your `~/.bashrc`:

    export RUST_TARGET_PATH=$HOME/proj/robigalia/sel4-targets

Followed by `source ~/.bashrc`, to set the variable in your running shell.

Make sure you have a recent Rust nightly installed. 
If you're using `rustup`, run something like:

    rustup default nightly-$DATE

The `$DATE` we use for CI can be found https://gitlab.com/robigalia/runner/blob/master/Dockerfile#L33[approximately here].

After that, just one last step for Rust:

    cargo install xargo && rustup component add rust-src

There are also some Python-related dependencies that need to be installed. If you have `pip` available, the following should be sufficient:

    pip install --user sel4-deps

Before building any robigalia low-level libraries you will need to `cd path/to/robigalia/sel4` and configure the sel4 kernel (see https://gitlab.com/robigalia/sel4[here]):

    make x64_qemu_defconfig

The robigalia low-level libraries read in these configuration values at build time and use them to correctly match the sel4 kernel ABI and for conditional compilation.

Now, you should be able to `cd path/to/robigalia/hello-world` and execute:

    ./build.sh --target x86_64-sel4-robigalia

To build the hello-world project.

To build the kernel, you'll additionally need `libxml2-utils`.

If you wish, you can also use the https://gitlab.com/robigalia/runner/tree/master[Docker container] that we use for CI. However, this hasn't been tested outside of the context of CI, and I don't know how to use it productively.

[[troubleshooting]]
Troubleshooting
---------------

If you tried following these steps but came across an error, *please* come
to our https://rbg.systems/contact.html[matrix room or IRC channel] so we
can help you, and add more details to this section. Thanks!
